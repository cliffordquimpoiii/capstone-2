//[SECTION] Dependencies and Modules
  const auth = require("../auth");
  const User = require('../models/User');
  const bcrypt = require("bcrypt");
  const dotenv = require("dotenv");

  
//[SECTION] Environment Setup
  dotenv.config();
  const salt = Number(process.env.SALT); 

//[SECTION] Functionalities [CREATE]
  module.exports.registerUser = (data) => {
  	let fName = data.firstName;
    let lName = data.lastName;
    let email = data.email;
    let passW= data.password;
    let gendr = data.gender;
    let mobil = data.mobileNo;
    let newUser = new User({
      firstName: fName,
      lastName: lName,
      email: email,
      password: bcrypt.hashSync(passW, salt),
      gender: gendr,
      mobileNo: mobil 
    }); 
    return User.findOne({email: email}).then(emailResult => {
      if(emailResult != null){
        return {message: 'Email Already Registered'}
      }

      else {
             return newUser.save().then((user, rejected) => {
            if (user) {
              return true; 
            } else {
              return {message: 'Failed to Register a new account'}; 
            }; 
        });
      }
    });
  };

//LOGIN


module.exports.loginUser = (req, res) => {


    console.log(req.body)

    User.findOne({email: req.body.email})
    .then(foundUser => {

      if(foundUser === null){
        return res.send({message :"User Not Found"});

      } else {

        const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

        console.log(isPasswordCorrect)

       
        if(isPasswordCorrect){

          return res.send({accessToken: auth.createAccessToken(foundUser)})

        } else {

          return res.send(false);
        }

      }

    })
    .catch(err => res.send(err))

};


//GET USER DETAILS
module.exports.getUserDetails = (req, res) => {

    console.log(req.user)

    /*
      Step/s:
      1. Find our logged in user's document from our db and send it to the client by its id

    */

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))
};


 
// [SECTION] Functionalities [RETRIEVE]
module.exports.displayAllUser = () => {
    return User.find({}).then(findOutcome =>{
      return findOutcome;
    })
  };



  module.exports.displayUserID = (id)  =>{
    return User.findById(id).then(idSearch => {
      return idSearch;
    });
  };

  module.exports.getAllAdminUser = () =>{
    return User.find({isAdmin: true}).then(resultofAdminUser =>{
      return resultofAdminUser;
    });
  };
//[SECTION] Functionalities [UPDATE]

module.exports.updateUser = (id, details) =>{
    let email = details.email;
    let password = details.password;

    let updatedUser ={
      email: email,
      password: password
    }

    return User.findByIdAndUpdate(id, updatedUser).then((userUpdated, err) => {
      if(err){
        return 'Failed to Update User';
      } else{
        return `User ${id} has been updated`;
      }
    });
  };

  module.exports.deactivateUser = (id)=>{
    let updates = {
      isAdmin: false
    }
    return User.findByIdAndUpdate(id, updates).then((archived, rejected)=>{
      if (archived) {
        return `User ${id} has been deactivated`

      } else {
        return 'Failed to archive Product'

      }
    });
  };

  module.exports.reactivateUser = (id)=>{
    let updates = {
      isAdmin: true
    }
    return User.findByIdAndUpdate(id, updates).then((reactivate, rejected)=>{
      if (reactivate) {
        return `User ${id} has been activated`

      } else {
        return 'Failed to reactivate User'

      }
    });
  };

//[SECTION] Functionalities [DELETE]

module.exports.deleteUser = (id) =>{
    return User.findByIdAndRemove(id).then((removedUser, err)=>{
      if(removedUser){
        return `User ${id} has been deleted`;
      }else{
        return 'Failed to Remove User'
      }
    });
  };