//[SECTION] Dependencies and Modules

	const Product = require('../models/Product');

//[SECTION] Functionality [CREATE]
	module.exports.createProduct = (info) =>{
		let pName = info.name;
		let pdesc = info.description;
		let pPrice = info.price;
		let newProduct = new Product({
			name: pName,
			description: pdesc,
			price: pPrice
			
		})
		return newProduct.save().then((savedProduct, error) => {
			if(error){
				return 'Failed to Save New Document';
			} else {
				return savedProduct;
			}
		}); 
	};

//[SECTION] Functionality [DISPLAY]
	module.exports.displayAllProduct = () => {
		return Product.find({}).then(findOutcome =>{
			return findOutcome;
		})
	};

	module.exports.displayProductID = (id)  =>{
		return Product.findById(id).then(idSearch => {
			return idSearch;
		});
	};

	module.exports.getAllActiveProduct = () =>{
		return Product.find({isActive: true}).then(resultofActiveProduct =>{
			return resultofActiveProduct;
		});
	};

//[SECTION] Funcionality [UPDATE]

	module.exports.updateProduct = (id, details) =>{
		let pName = details.name;
		let pDesc = details.description;
		let pPrice = details.price;

		let updatedProduct ={
			name: pName,
			description: pDesc,
			price: pPrice
		}

		return Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
			if(err){
				return false;
			} else{
				return true;
			}
		});
	};


	module.exports.deactivateProduct = (id)=>{
		let updates = {
			isActive: false
		}
		return Product.findByIdAndUpdate(id, updates).then((archived, rejected)=>{
			if (archived) {
				return true

			} else {
				return false

			}
		});
	};

	module.exports.reactivateProduct = (id)=>{
		let updates = {
			isActive: true
		}
		return Product.findByIdAndUpdate(id, updates).then((reactivate, rejected)=>{
			if (reactivate) {
				return true

			} else {
				return false

			}
		});
	};

	//[SECTION] DELETE
	module.exports.deleteProduct = (id) =>{
		return Product.findByIdAndRemove(id).then((removedCourse, err)=>{
			if(removedCourse){
				return true;
			}else{
				return false;
			}
		});
	};