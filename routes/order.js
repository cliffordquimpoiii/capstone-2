//[SECTION] Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/order');
	const auth = require("../auth");
    const {verify, verifyAdmin} = auth
//[SECTION] Routing Component 
	const route = exp.Router(); 

//[SECTION] Routes get 
route.get('/:id',  (req, res)=> {

		let orderId = req.params.id
		controller.displayOrderID(orderId).then(outcome =>{
			res.send(outcome);
		});
	});
route.get("/", verify, controller.getOrders);
//[SECTION] post
route.post("/order", verify, controller.checkOutOrder);
 //  route.post('/order', verify,  (req, res)=>{
	// 	let data = req.body
	// 	let user = req.user
	// 	controller.checkOutOrder(data, user).then(outcome =>{
	// 		res.send(outcome);
	// 	});
	// });




//[SECTION] Expose Route System
  module.exports = route; 
