//[SECTION] Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/product');
	const auth = require("../auth");
    const {verify, verifyAdmin} = auth
//[SECTION] Routing Component 
	const route = exp.Router(); 

//[SECTION] Routes POST

	route.post('/create', verify, verifyAdmin, (req, res)=>{
		let data = req.body
		controller.createProduct(data).then(outcome =>{
			res.send(outcome);
		});
	});

//[SECTION] Routes GET
	route.get('/all',  (req, res)=>{
	controller.displayAllProduct().then(outcome => {
		res.send(outcome);
		});
	});

	route.get('/:id',  (req, res)=> {

		let productId = req.params.id
		controller.displayProductID(productId).then(outcome =>{
			res.send(outcome);
		});
	});

	route.get('/',   (req, res)=>{
		controller.getAllActiveProduct().then(outcome =>{
			res.send(outcome);
		});
	});

//[SECTION] Routes PUT
	route.put('/:id', verify, verifyAdmin,  (req, res)=>{
		let id = req.params.id;
		let details = req.body

		let pName = details.name;
		let pDesc = details.description;
		let pPrice = details.price;
			if(pName !=='' && pDesc !== '' && pPrice !== '' ){
				controller.updateProduct(id, details).then(outcome => {
					res.send(outcome);
				});
			} else{
				res.send({message: 'Incorrect Input, Make sure details are complete'});
			}
	});

	route.put('/:id/archive', verify, verifyAdmin,  (req, res)=>{
		let productId = req.params.id;

		controller.deactivateProduct(productId).then(outcome => {
			res.send(outcome);
		});
	});

	route.put('/:id/reactivate',  (req, res)=>{
		let productId = req.params.id;

		controller.reactivateProduct(productId).then(outcome=>{
			res.send(outcome);
		});
	});


//[SECTION] Routes DEL

 route.delete('/:id/delete', verify, verifyAdmin,  (req, res)=>{
 	let productId = req.params.id;
 	controller.deleteProduct(productId).then(outcome =>{
 		res.send(outcome);
 	})
 });
	//[SECTION] Expose Route System
  module.exports = route; 