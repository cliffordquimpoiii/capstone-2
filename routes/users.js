//[SECTION] Dependencies and Modules
  const exp = require("express"); 
  const controller = require('./../controllers/users.js');
   const auth = require("../auth");
  const {verify, verifyAdmin} = auth

//[SECTION] Routing Component
  const route = exp.Router(); 

//[SECTION] Routes-[POST]
  route.post('/register', (req, res) => {
    let userDetails = req.body; 
    controller.registerUser(userDetails).then(outcome => {
       res.send(outcome);
    });
  });

    route.post("/login", controller.loginUser);



//[SECTION] Routes-[GET]

route.get('/all', verify,  (req, res)=>{
  controller.displayAllUser().then(outcome => {
    res.send(outcome);
    });
  });


route.get("/getUserDetails", verify, controller.getUserDetails)




  route.get('/:id', (req, res)=> {

    let userId = req.params.id
    controller.displayUserID(userId).then(outcome =>{
      res.send(outcome);
    });
  });

  route.get('/', verify, verifyAdmin,  (req, res)=>{
    controller.getAllAdminUser().then(outcome =>{
      res.send(outcome);
    });
  });
//[SECTION] Routes-[PUT]
route.put('/:id', verify, verifyAdmin,  (req, res)=>{
    let id = req.params.id;
    let details = req.body

    let email = details.email;
    let password = details.password;
      if(email !=='' && password !== '' ){
        controller.updateUser(id, details).then(outcome => {
          res.send(outcome);
        });
      } else{
        res.send('Incorrect Input, Make sure details are complete');
      }
  });

  route.put('/:id/archive', verify, verifyAdmin, (req, res)=>{
    let userId = req.params.id;

    controller.deactivateUser(userId).then(outcome => {
      res.send(outcome);
    });
  });

  route.put('/:id/reactivate', verify, verifyAdmin,  (req, res)=>{
    let userId = req.params.id;

    controller.reactivateUser(userId).then(outcome=>{
      res.send(outcome);
    });
  });


//[SECTION] Routes-[DELETE]


 route.delete('/:id/delete', verify, verifyAdmin,  (req, res)=>{
  let userId = req.params.id;
  controller.deleteUser(userId).then(outcome =>{
    res.send(outcome);
  })
 });
//[SECTION] Expose Route System
  module.exports = route; 
